---
id: policies
title: SEKnFind Policies
sidebar_label: Policies
---
Effective Date: 11/3/2008 | Revised: 11/16/2018

## POLICY AVAILABILITY

This document is stored electronically on the SEKLS website under Services \>
SEKnFind \> ‘For Staff’ \> Policies (http://sekls.org/services/seknfind)

In libraries with more than one staff member, please make sure all staff can
locate and refer to this document at any given time.

In libraries with only one staff member, please make sure one or more board
members can locate and refer to this document at any given time.

## 1. PATRONS

### 1. One Patron – One Account

1.  Policy Statement

    Each patron will have only one account within the SEKnFind Consortium. No
    new account is to be created for existing patrons.  
      
    The home library will be determined by the patron’s library of first
    registration.

    The patron may choose to change his home library at any time. This most
    commonly occurs when the patron moves, or the patron wants to pick up their
    holds at a different library by default.

    A new physical card may be issued for the existing patron account.

2.  Procedures

    Whenever a patron approaches a library for a new card, circulation staff
    will search the patron database and verify that an account does not already
    exist for said patron.

    If the patron is found, account information including address, phone, and
    email should be updated at the patron’s discretion. Home library may be
    updated at this time as well.

    If the patron has multiple accounts, they should be merged onto the patron
    record with the fullest and most up-to-date information. Merging duplicate
    accounts combines all checkouts, fines, holds, and histories under the
    chosen account. Once merged, all outstanding problems must be resolved
    before usage may resume. **Merging cannot be undone**, so please make
    certain that the accounts belong to the same person before conducting a
    merge.

3.  Management of outstanding account issues:

    1.  *Overdue Materials* – The library should offer to return the items to
        the owning library via the courier.

    2.  *Fines* – Refer to the Fines Collection policy (2.3).

    3.  *Lost Materials* – Patrons should be directed back to the original
        lending library to settle these matters.

### 2. Patron Confidentiality

1. Policy Statement

	Patron confidentiality is paramount at all times for patron and circulation
	records pertaining to identifiable individuals, whether the patron is your
	library’s patron or not. No details will be shared with any person other than
	the patron himself regarding address, age, materials checked out or ordered,
	fines owed, computer use, or any other information from the patron record. The
	same is true of circulation history on an item.

2. Procedures

	If patron information is requested by a member of the police force, no
	information is to be provided. Police should be informed that the administration
	of SEKnFind rests with the Southeast Kansas Library System (SEKLS), and police
	requests for information should be referred to the director of SEKLS, or if
	(s)he is not available, other SEKLS staff.

### 3. Patron Self-Registration

1. Policy Statements

	Patrons may register online via the OPAC.  
  
	Self-registered patrons are allowed 2 holds; checkouts are restricted until the
	account is verified.  
  
	Self-registered accounts are automatically deleted after 14 days if the account
	is not verified and the patron category is not updated.

2. Procedures

	When a self-registered patron approaches the desk for the first time, patron
	information should be verified as correct. The patron category *must* be changed
	from Self-Registered to an appropriate category (Adult In, Juvenile Out, etc.)
	or the account *will* be deleted.

### 4. Best Practices for Creating Patron Accounts

1. Uploading patron images

	To aid in identification and security of patrons who may request cards at
	multiple locations, libraries may upload a picture of the patron to the
	appropriate section of the patron record. This is voluntary and if the patron
	refuses, a notice will be loaded into the record in place of a picture to denote
	that the patron has already been asked and declined the inclusion of an image to
	their account.

2. Username and password

	Take time to set a username and password for the patron at the time the account
	is created to allow patrons access to their account via the OPAC. Usernames must
	be unique. Passwords must be at least 3 characters in length.

3. Linking parent and child accounts

	Child accounts may be linked to parent/guarantor accounts during account
	creation. This allows staff to see checkouts on the child card via the parent
	account.

## 2. CIRCULATION

### 1. System-Wide Circulation and Fine Settings

1. Policy Statement

	Circulation and fine rules are constrained by system-wide settings.  
  
	Settings:  
	A. Maximum amount a patron can owe before checkouts and holds are blocked: \$10  
	B. Maximum fine amount per patron: \$9999  
	C. Maximum number of holds a patron can place: 150  
	D. Long overdue status is added to checked out items at 99 days overdue.  
	E. Items circulate based on the rules of the *transacting* library; transferred materials will circulate according to the rules of the receiving library, not the owning library.  
	F. Renewal periods begin on the date the renewal takes place, not the date the item is due.

### 2. Local Circulation and Fine Settings

1. Policy Statement

	Local rules are set via the circulation and fine rules in the Administration
	area. Rules are applied to item type/patron category combinations based on local
	circulation policies.  
  
	Local Circulation/Fine Settings:  
	A. Current number of checkouts  
	B. Length of loan period (in days or hours)  
	C. Hard due date (makes everything in that rule due on or before the specified date)  
	D. Fine amount, if any  
	E. Fine grace period, if any  
	F. Number of renewals  
	G. Renewal period (can be longer or shorter than the regular loan period)  
	H. Number of holds (limits holds within the system-wide 150 hold maximum)  
	I. Overdue fines cap (halts fines at a specified dollar amount)

### 3. Collection of Fines

1. Policy Statement

	Staff at any library should collect fines owed at any other library when the
	patron is present and wishes to pay or must pay to reactivate his privileges.
	Fines must be below \$10 for patrons to check out items.

2. Procedures

	A. If the collected fine is more than \$10, forward the collected money to the library it is owed.  
	B. If the collected fine is less than \$10, forward the amount or keep it at your library, depending on local policy.

### 4. Renewal of Items Abroad

1. Policy Statement

	When a patron asks to renew an item which is eligible for renewal but which was
	checked out at a different library, it is acceptable to renew the item for that
	patron. If the renewal requires an override of the renewal limit, the patron
	should be referred back to the library where they checked out the book.

### 5. Return of Items Abroad

1. Policy Statement

	If items are returned at a location different from the location at which they
	were initially checked out, staff should check the items in then follow any
	prompts that Koha displays (print, transfer and confirm a hold; return to a
	location; etc.). In the event that a barcode is old/too short, refer to the
	[Barcode Pre-appending Documentation](http://sekls.net/Documents/SEKnFind%20Item%20Prepend%20Codes.pdf).

### 6. New Item Sharing

1. Policy Statement

	SEKLS strongly encourages libraries to adhere to the best practice of letting
	the system work; that is, of sending materials to fill holds as soon as the
	request comes up on the library’s holds queue.

	A series of ‘New’ itemtypes maintains the flow of sharing while giving members a
	choice of immediate or delayed sharing of new materials.

	Materials are considered ‘new’ for six weeks based on the date they are added to
	the catalog.

	Libraries who share new items immediately face no restrictions. Staff and
	patrons at these libraries may place holds on new items as soon as they are
	cataloged.

	Libraries who delay sharing of new materials for a six week period face barriers
	to borrowing ‘new’ items to account for non-lending of ‘new’ items. Library
	staff and patrons at these libraries cannot place holds on the ‘new’ materials
	of other libraries.

	Please note: Local holds prioritization is active for all libraries. Regardless
	of your ‘new sharing’ setup, your items will give priority to your patrons’
	holds.

2. Procedures  
	A. All libraries will use the appropriate ‘New’ itemtype on any new item added to the catalog.  
	B. All materials marked ‘New’ will automatically update to a corresponding itemtype six weeks after addition to the catalog.
	C. All items past the six-week mark will be shared freely.
	D. If you choose to delay sharing, you must contact the SEKnFind Coordinator for setup.

### 7. Best Practices for Circulation

1. Scanning

	Scan the barcode then look: the web-based system is dependent on your Internet speed and is often slower than you expect.

2. Checkout Slips

	You must click on the “print” button then “print slip” or click “check out” when the checkout box is empty to print a slip; it is not automatic.  
  
	Checkout slips can present privacy concerns based on how much information is
	displayed on the slip. Each library may customize their slips individually;
	contact the SEKnFind coordinator for assistance on setup and modification.

## 3. HOLDS & TRANSFERS

### 1. Placing Holds

1.  Policy Statements

    Record-level (Next Available) holds are to be utilized in nearly every
    situation.  
      
    Item-specific holds lock requests to only one item which causes the hold to
    be filled slower and increases the potential for a hold to be abandoned.
    These should be used only in special cases.

    Item-specific holds are not allowed in the OPAC.

### 2. Pulling Holds

1. Policy Statement

	Each library should pull holds at least once each day the library is open. It is
	permissible that holds be pulled only on days that a library receives courier,
	as long as the pulls are done prior to the courier’s arrival those days.  
  
	The holds queue regenerates every 15 minutes.

	Holds must be pulled *and* filled prior to the next queue regeneration to
	prevent overlap. For example, if you run the holds queue at 8am, all holds must
	be pulled, checked in and transferred/confirmed prior to 11:50am to avoid
	conflicts with other libraries that have the item.  
  
	Items that cannot be found must be marked missing. If your item is the only one
	on the record, the library of the requesting patron must be notified.

### 3. Returning Holds

1. Policy Statement

	Holds not picked up by the patron within five days should be moved on.

2. Procedures

	1. The hold for the patron that did not pick up the item must be deleted.

	2. The item must then be checked in.

		- If the item has more holds, print slip/transfer/confirm and send the item onward  
		- If the item does not have holds, return it to its home library

### 4. Items Lost In Transit

1. Policy Statement

	Items lost in transit to and from your library must be investigated and accounted for.

2. Procedures

	1. Find problems using the transfers-to-receive report weekly
	2. Check your shelves for the item  
	3. If not found, have the borrowing/lending/owning library check their shelves  
	4. Determine responsibility for replacing the item  
	5. Request reimbursement or request to be billed by the owning library

3. Responsibility for Replacement

	As soon as an item departs a lending library, it becomes the responsibility of
	the borrowing library. If the item never arrives at the borrowing library, or
	never finds its way back to the lending library, the borrowing library is
	responsible for replacing the item either via reimbursement or materials
	replacement claim.

## 4. CATALOGING

### 1. Support

1. Policy Statement

	All cataloging-related issues should be directed to the SEKLS cataloging staff.  
  
	Cataloging staff are available to perform specialty cataloging tasks such as
	original records that are not owned by other libraries.  
  
	Additional policies and practices concerning cataloging within the SEKnFind
	consortium can be found in the [online cataloging
	manual](http://sekls.org/services/cataloging/manual).

## 5. REPORTS

### 1. Weekly Reports

1. Policy Statement

	The following reports should be run and worked on weekly:
	- Transfers-to-Receive
	- Orphaned Holds

### 2. Monthly Reports

1. Policy Statement

	An email will be sent at the start of each month with useful reports for keeping various parts of the catalog clean.

### 3. Custom Reports

1. Policy Statement

	Many reports are listed in the reports module under More \> Reports \> Use
	saved. For help locating a report or to have a report written, please contact
	the SEKnFind coordinator at SEKLS.

## 6. ADMINISTRATION

### 1. Staff Permissions

1.  Policy Statement

	For smaller libraries (one or two staff members), SEKLS will set up staff
	permissions. In these cases, please notify the SEKnFind coordinator of personnel
	changes so that permissions can be removed and assigned as necessary.  
  
	For larger libraries, the library director or person designated by the director
	will set up staff permissions. We ask directors to observe the following
	limitations:

	- No one but the director or the person designated by the director should be able to set permissions for staff.
	- No one should be given superlibrarian status.
	- No one but the director should be given the permission of “parameters”.
	- No system preferences should be changed.
	- The “edit_catalogue” permission should only be granted after SEKLS staff has trained on cataloging.

### 2. Calendar Setup

1. Policy Statement

	Each library will define its own calendar of closings each year.  
  
	Proper and timely calendar setup ensures that:  
		A. Fines will not accrue on days the library is closed.  
		B. The holds queue will not generate on days the library is closed.  
		C. Items will not come due on days the library is closed.

### 3. Overdue Notice Setup

1. Policy Statement

	Each library will write and define triggers for their own overdue notices. Up to
	three notices can be created per library. Overdue notices have the ability to
	restrict patron accounts when sent.

### 4. Purchase Suggestion Management

1. Policy Statement

	Each library will manage its own purchase suggestions at least once a week.

### 5. Tag/Comment Moderation

1. Policy Statement

	Tags and comments will be moderated by SEKLS staff.

### 6. Patrons Pending Modification

1. Policy Statement

	Patrons are able to request modifications to their account information via the
	OPAC. Staff at the home library of the patron requesting modification will
	approve or deny the changes.

### 7. Posting News Items

1. Policy Statement

	Only the director or designated staff persons will be allowed to post news
	items; this requires a special permission setting.

	News items of local interest may be posted at *your library* on the OPAC, staff
	client, or both. This might include storytimes, book discussion groups, and
	other events aimed at your library’s individual community.

	Postings of general interest may be displayed at *all libraries* on the OPAC,
	staff client, or both. Annual book sales, Kansas Humanities Council speakers,
	author appearances, or other non-recurring events that may have a wider appeal
	beyond your community can be posted to *all libraries*.

	SEKLS staff will monitor news posts and adjust where they display if the
	audience is deemed inappropriate.

### 8. Director Transitions

1. Policy Statement

	In the event of a director’s departure from a library, certain standards must be maintained.

	*Staff Permissions* - Representatives from the affected library should
	notify SEKLS staff immediately of any changes in directorship so that
	SEKnFind permissions may be adjusted. If the person(s) maintaining operation
	of the library in the interim does not have staff access, a circulation or
	substitute account for that particular library should be utilized.

	*Participation* - During transitional periods where the director position is
	vacant, member libraries are expected to maintain a minimum level of
	participation in the consortium. Activities that must be performed daily,
	with or without a director include:
	- Checking out items
	- Renewing items
	- Checking in items
	- Collecting fines (if applicable)
	- Placing holds for patrons
	- Pulling the holds queue
	- Preparing items for courier pickup
	- Opening packages from courier delivery
	- Notifying patrons of waiting holds
	- Checking library email
	- Notifying patrons of overdue items

	In regards to cataloging, if remaining staff have not had cataloging
	training from SEKLS, items needing cataloged during the transitional period
	should be sent via courier to the SEKLS offices to be cataloged until the
	new director is hired and trained.

	*Closings* - It is preferred that the library remain operational while
	director vacancies are being filled. If the library must close in the
	interim, SEKLS staff *must* be notified so that consortial activity for the
	library may be suspended.

## 7. SUPPORT

### 1. Reporting Problems

1.  Policy Statement

	Issues and bugs should be directed to the SEKnFind Coordinator.  
  
	All contact with ByWater Solutions will be made through the SEKnFind
	Coordinator.